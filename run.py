#!/usr/bin/env python3
import i3ipc


def turn_output_off(i3: i3ipc.Connection, output_name: str):
    i3.command(f"output {output_name} dpms off")


def turn_output_on(i3: i3ipc.Connection, output_name: str):
    i3.command(f"output {output_name} dpms on")


def on_fullscreen_mode(i3: i3ipc.Connection, e: i3ipc.events.WindowEvent):
    if e.container.app_id != "mpv":
        return

    outputs = i3.get_outputs()

    if e.container.fullscreen_mode == 1:
        for o in outputs:
            if not o.focused:
                turn_output_off(i3, o.name)
    else:
        for o in outputs:
            if not o.focused:
                turn_output_on(i3, o.name)


def on_close(i3: i3ipc.Connection, e: i3ipc.events.WindowEvent):
    if e.container.app_id != "mpv":
        return

    outputs = i3.get_outputs()
    for o in outputs:
        if not o.focused and not o.dpms:
            turn_output_on(i3, o.name)


def main():
    i3 = i3ipc.Connection()
    i3.on("window::fullscreen_mode", on_fullscreen_mode)
    i3.on("window::close", on_close)
    i3.main()


if __name__ == "__main__":
    main()
