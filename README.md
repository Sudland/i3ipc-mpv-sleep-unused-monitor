# i3ipc mpv sleep unused monitor

A simple i3ipc Python script that puts all monitors, not used by mpv, to sleep in sway/i3.
